package com.raywenderlich.android.cocktails.game.model

import org.junit.Assert
import org.junit.Test

class ScoreUnitTests {

    @Test
    fun whenIncrementingScore_shouldIncrementCurrentScore() {
        val game = Game(emptyList(), Score(0))

        game.incrementScore()

        Assert.assertEquals("Current score should have been 1", 1, game.curretScore)
    }

    @Test
    fun whenIncrementingScore_aboveHighScore_shouldAlsoIncrementHighScore() {
        val game = Game(emptyList(), Score(0))

        game.incrementScore()

        Assert.assertEquals(1, game.highestScore)
    }

    @Test
    fun whenIncrementingScore_belowHighScore_shouldNotIncrementHighScore() {
        val game = Game(emptyList(), Score(10))

        game.incrementScore()

        Assert.assertEquals(10, game.highestScore)
    }
}